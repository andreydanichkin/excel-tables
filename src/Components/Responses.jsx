import React, {Component} from 'react';
import IconLabelButtons from "./ButtonsGroup";
import {keys} from "@material-ui/core/styles/createBreakpoints";

class Responses extends Component {

    render() {

        const ic4pro_notifiers = [
            {
                "key":"initiator",
                "description":"Initiator",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            },
            {
                "key":"keyOfficers",
                "description":"Key Officers",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            },
            {
                "key":"auditTeam",
                "description":"Audit Team",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            },
            {
                "key":"reviewers",
                "description":"Reviewers",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            },
            {
                "key":"approver",
                "description":"Approver",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            }
        ]


        let tableTemplate;
        let tableHeader;

        function makeColumns(row) {
            return <tr><td>X</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td></tr>
        }

        tableTemplate = ic4pro_notifiers.map((row, i) => {
            return <tr key={i}>{makeColumns(row)}</tr>
        })


        function makeHeader(head) {
            return <tr><th>{head.key}</th></tr>
        }

        const tHeader = <thead className='head'>
                            <tr>
                                <td>initiator</td>
                                <td>keyOfficers</td>
                                <td>auditTeam</td>
                                <td>reviewers</td>
                                <td>approver</td>
                            </tr>
                        </thead>



        tableHeader = ic4pro_notifiers.map((head, i) => {
            const keys = Object.keys(head);
            return <tr key={i}>{makeHeader(head)}</tr>
        })


        return (
            <div>
                <IconLabelButtons />
                <table>
                    <thead className="cell">
                    {tHeader}
                    </thead>
                    <tbody>
                    {tableTemplate}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Responses;