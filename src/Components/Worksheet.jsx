import React, {Component} from 'react';

class Worksheet extends Component {

    render() {

        const data = [
            {
                "key":"initiator",
                "description":"Initiator",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            },
            {
                "key":"keyOfficers",
                "description":"Key Officers",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            },
            {
                "key":"auditTeam",
                "description":"Audit Team",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            },
            {
                "key":"reviewers",
                "description":"Reviewers",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            },
            {
                "key":"approver",
                "description":"Approver",
                "recordDate":"20200603",
                "recordTime":"10:30:40",
                "operator":"adroit",
                "operation":"edit",
                "workstation":"11.20.10.1",
                "recordCounter":"1"
            }
        ]

        let tableTemplate;

        function makeColumns(row, key) {
            return <tr><td>{row.key}</td>
                <td>{row.description}</td>
                <td>{row.recordDate}</td>
                <td>{row.recordTime}</td>
                <td>{row.operator}</td>
                <td>{row.operation}</td>
                <td>{row.workstation}</td>
                <td>{row.recordCounter}</td></tr>
        }

        tableTemplate = data.map((row, i) => {
            return <tr key={i}>{makeColumns(row, i)}</tr>
        })

        return (
            <div>
                <table>
                    <tbody>
                    {tableTemplate}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Worksheet;