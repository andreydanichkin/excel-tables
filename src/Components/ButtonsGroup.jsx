import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

export default function ContainedButtons() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Button variant="contained" >Create</Button>
            <Button variant="contained" color="primary">
                Edit
            </Button>
            <Button variant="contained" color="secondary">
                View
            </Button>
            <Button variant="contained" disabled>
                Delete
            </Button>
            <Button variant="contained" color="primary" href="#contained-buttons">
                Link
            </Button>
        </div>
    );
}
