import React, {Component} from 'react';
import IconLabelButtons from "./ButtonsGroup";

class Findings extends Component {
    render() {

        const entity = [
            {
                "entityId":"utilityBills",
                "description":"Utility Bills Details"
            },
            {
                "entityId":"identification",
                "description":"Evidence of identification"
            },
            {
                "entityId":"refere",
                "description":"References"
            },
            {
                "entityId":"mematTrueCopy",
                "description":"Certified True Copy of MEMAT"
            },
            {
                "entityId":"boardResolution",
                "description":"Board Resolution"
            },
            {
                "entityId":"kycReport",
                "description":"Know Your Customer Report"
            },
            {
                "entityId":"visitationReport",
                "description":"Visitation Report"
            },
            {
                "entityId":"specSignatureCard",
                "description":"Specimen Signature Card"
            }
        ]

        let tableTemplate;

        const tableHeader = <thead className='head'>
                                <tr>
                                    <td>entityId</td>
                                    <td>description</td>
                                </tr>
                            </thead>

        function makeColumns(row) {
            return <tr><td>{row.entityId}</td><td>{row.description}</td></tr>
        }

        tableTemplate = entity.map((row, i) => {
            return <tr key={i}>{makeColumns(row)}</tr>
        })

        return (
            <div>
                <IconLabelButtons />
                <table>
                    <thead>
                    {tableHeader}
                    </thead>
                    <tbody>
                    {tableTemplate}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default Findings;