import React, {Component} from 'react';
import './App.css';
import Worksheet from "./Components/Worksheet";
import Findings from "./Components/Findings";
import Responses from "./Components/Responses";
import FullWidthTabs from "./Components/Navigation";

class App extends Component {
    render() {
        return (
            <div>
                <FullWidthTabs />
            </div>
        );
    }
}

export default App;